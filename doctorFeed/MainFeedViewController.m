//
//  MainFeedViewController.m
//  doctorFeed
//
//  Created by Artem Peskishev on 06.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "MainFeedViewController.h"
#import "PublicationViewController.h"
#import "PublicationTableViewCell.h"
#import "NSAttributedString+DDHTML.h"
#import "IndexedCollectionView.h"
#import "AttachmentCollectionViewCell.h"
#import "Attachment.h"
#import <IDMPhoto.h>
#import <IDMPhotoBrowser.h>
#import <UIScrollView+SVPullToRefresh.h>
#import <UIScrollView+SVInfiniteScrolling.h>
#import "PublicationTypeToCreate.h"
#import "DFTabBarViewController.h"
#import "DFPDFViewerViewController.h"
#import "DFSpecialty.h"
#import "PublicationFactory.h"
#import "Publication.h"

#import "XCDYouTubeVideoPlayerViewController.h"

@interface MainFeedViewController ()
@property NSMutableArray *feedObjects;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *profileNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UIView *profileView;

@property Doctor *user;
@property Publication *selectedPublication;
@property NSMutableArray *publicationTypes;
@property NSMutableArray *specialtiesArray;

//filtered
@property (weak, nonatomic) IBOutlet UIButton *specialtiesButton;
@property (weak, nonatomic) IBOutlet UIButton *popularButton;
@property (weak, nonatomic) IBOutlet UITextField *chooseFilterTextfield;
@end

@implementation MainFeedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    self.feedObjects = [[NSMutableArray alloc]init];
    self.view.userInteractionEnabled = NO;
    
    [SVProgressHUD showWithStatus:@"Загружаю новые публикации"];
    
    if (self.tabBarController.selectedIndex == 0)
    {
        [self setupViewForMainFeed];
    } else if (self.tabBarController.selectedIndex == 2)
    {
        [self setupViewForFilteredFeed];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openPDF:) name:@"openPDF" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openVideo:) name:@"playVideo" object:nil];
    
    [self setupTabbar];
    [self addPullToRefresh];
    [self addInfiniteScrolling];
}

- (void)setupViewForMainFeed
{
    [[APIManager sharedManager]loginWithMail:@"asd@asd.ru" pwd:@"asd" completionBlock:^(NSDictionary *answer, NSError *error) {
        self.user = [[Doctor alloc]initUserDoctorWithDict:answer[@"doctor"]];
        
        self.publicationTypes = [NSMutableArray new];
        
        for (NSDictionary *dict in answer[@"publicationTypes"])
        {
            PublicationTypeToCreate *publicationType = [PublicationTypeToCreate new];
            [publicationType import:dict];
            [self.publicationTypes addObject:publicationType];
        }
        
        DFTabBarViewController *tabbarVC = (DFTabBarViewController *)self.tabBarController;
        tabbarVC.publicationTypes = self.publicationTypes;
        tabbarVC.userDoctor = self.user;
        
        [self setupUserProfile];
        [self loadFeedFromDate:[NSDate date]];
    }];
}

-(void)setupViewForFilteredFeed
{
    [self.specialtiesButton setBackgroundColor:[UIColor colorWithRed:0.34 green:0.63 blue:0.78 alpha:1.00]];
    [self.specialtiesButton setTintColor:[UIColor whiteColor]];
    self.specialtiesButton.layer.cornerRadius = 4;
    
    [self.popularButton setBackgroundColor:[UIColor clearColor]];
    [self.popularButton setTintColor:[UIColor colorWithRed:0.05 green:0.37 blue:0.55 alpha:1.00]];
    self.popularButton.layer.cornerRadius = 4;
    
    [self loadFilteredFeedFromDate:[NSDate date]];
    
    DFTabBarViewController *tabbarVC = (DFTabBarViewController *)self.tabBarController;
    self.user = tabbarVC.userDoctor;
    
    self.chooseFilterTextfield.rightViewMode = UITextFieldViewModeAlways;
    self.chooseFilterTextfield.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dropdown.png"]];
    
    [self setupUserProfile];
}

- (void)addPullToRefresh
{
    [self.tableView addPullToRefreshWithActionHandler:^{
        [[APIManager sharedManager]loadFeedFromDate:[NSDate date] completionBlock:^(NSDictionary *answer, NSError *error) {
            self.feedObjects = [NSMutableArray new];
            for (NSDictionary *item in answer[@"items"])
            {
                Publication *publication = [PublicationFactory createPublication:FeedType withDict:item];
                if (publication.hasFullContent)
                {
                    publication.annotation = [publication.annotation stringByAppendingString:@"..."];
                }
                [self.feedObjects addObject:publication];
            }
            [self.tableView.pullToRefreshView stopAnimating];
            [self.tableView.infiniteScrollingView stopAnimating];
            [self.tableView reloadData];
            [SVProgressHUD dismiss];
        }];
    }];
}

- (void)addInfiniteScrolling
{
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        Publication *lastPublication = [self.feedObjects lastObject];
        [self loadFeedFromDate:lastPublication.createdAt];
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showPublication"])
    {
        PublicationViewController *publicationVC = segue.destinationViewController;
        publicationVC.publication = self.selectedPublication;
    }
}

//==================------------------
#pragma mark - Notifications
//==================------------------

- (void)openPDF:(NSNotification *)notification
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DFPDFViewerViewController *pdfVC = (DFPDFViewerViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PDFViewer"];
    pdfVC.pdfURL = notification.object;
    [self.navigationController pushViewController:pdfVC animated:YES];
}

- (void)openVideo:(NSNotification *)notification
{
    NSString *videoId = [Helper extractYoutubeID:notification.object];
    XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:videoId];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
}

//==================------------------
#pragma mark - Feed
//==================------------------

- (void)loadFeedFromDate:(NSDate *)date
{
    [[APIManager sharedManager]loadFeedFromDate:date completionBlock:^(NSDictionary *answer, NSError *error) {
        for (NSDictionary *item in answer[@"items"])
        {
            Publication *publication = [PublicationFactory createPublication:FeedType withDict:item];
            if (publication.hasFullContent)
            {
                publication.annotation = [publication.annotation stringByAppendingString:@"..."];
            }
            [self.feedObjects addObject:publication];
        }
        [self.tableView.pullToRefreshView stopAnimating];
        [self.tableView.infiniteScrollingView stopAnimating];
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled = YES;
    }];
}

- (void)loadFilteredFeedFromDate:(NSDate *)date
{
    [[APIManager sharedManager]loadFeedFromDate:date completionBlock:^(NSDictionary *answer, NSError *error) {
        for (NSDictionary *item in answer[@"items"])
        {
            Publication *publication = [PublicationFactory createPublication:FeedType withDict:item];
            if (publication.hasFullContent)
            {
                publication.annotation = [publication.annotation stringByAppendingString:@"..."];
            }
            [self.feedObjects addObject:publication];
        }
        [self.tableView.pullToRefreshView stopAnimating];
        [self.tableView.infiniteScrollingView stopAnimating];
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
        self.view.userInteractionEnabled = YES;
    }];
}

//==================------------------
#pragma mark - Feed Filtering
//==================------------------

- (IBAction)specialtyButtonTapped:(id)sender
{
    [self.specialtiesButton setBackgroundColor:[UIColor colorWithRed:0.34 green:0.63 blue:0.78 alpha:1.00]];
    [self.specialtiesButton setTintColor:[UIColor whiteColor]];
    
    [self.popularButton setBackgroundColor:[UIColor clearColor]];
    [self.popularButton setTintColor:[UIColor colorWithRed:0.05 green:0.37 blue:0.55 alpha:1.00]];
}
- (IBAction)popularButtonTapped:(id)sender
{
    [self.popularButton setBackgroundColor:[UIColor colorWithRed:0.34 green:0.63 blue:0.78 alpha:1.00]];
    [self.popularButton setTintColor:[UIColor whiteColor]];
    
    [self.specialtiesButton setBackgroundColor:[UIColor clearColor]];
    [self.specialtiesButton setTintColor:[UIColor colorWithRed:0.05 green:0.37 blue:0.55 alpha:1.00]];
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return NO;  // Hide both keyboard and blinking cursor.
}

//==================------------------
#pragma mark - Interface Init
//==================------------------

-(void)setupTabbar
{
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor whiteColor], NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor whiteColor], NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateSelected];
    
    UITabBarController *tabBarController = self.tabBarController;
    UITabBar *tabBar = tabBarController.tabBar;
    
    UITabBarItem *firstTab = [tabBar.items objectAtIndex:0];
    
    firstTab.image = [[UIImage imageNamed:@"feed"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    firstTab.selectedImage = [[UIImage imageNamed:@"feed_selected"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UITabBarItem *secondTab = [tabBar.items objectAtIndex:1];
    
    secondTab.image = [[UIImage imageNamed:@"add"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    secondTab.selectedImage = [[UIImage imageNamed:@"add_selected"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UITabBarItem *thirdTab = [tabBar.items objectAtIndex:2];
    
    thirdTab.image = [[UIImage imageNamed:@"filter"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    thirdTab.selectedImage = [[UIImage imageNamed:@"filter_selected"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

}

-(void)setupUserProfile
{
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:self.profileView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -9;
    self.navigationItem.leftBarButtonItems = @[negativeSpacer,leftBarItem];
    
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2;
    self.profileImageView.clipsToBounds = YES;
    
    self.profileImageView.layer.borderWidth = 1.0f;
    self.profileImageView.layer.borderColor = [UIColor colorWithRed:0.91 green:0.96 blue:1.00 alpha:1.0].CGColor;

    [self.profileNameLabel setText:self.user.shortName];
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:self.user.avatarUrl]];
}

//==================------------------
#pragma mark - Indexed Collection View
//==================------------------

-(NSInteger)collectionView:(IndexedCollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{

    Publication *publication = self.feedObjects[collectionView.index];

    return publication.attachmentsArray.count;
}



-(UICollectionViewCell *)collectionView:(IndexedCollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AttachmentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCollectionViewCell" forIndexPath:indexPath];
    
    Publication *publication = self.feedObjects[collectionView.index];

    Attachment * attachment = publication.attachmentsArray[indexPath.item];
    
    [cell setCellWithAttachment:attachment];
    
    return cell;
}

-(CGSize)collectionView:(IndexedCollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Publication *publication = self.feedObjects[collectionView.index];
    Attachment * attachment = publication.attachmentsArray[indexPath.item];
    CGSize itemSize = CGSizeMake(0, 0);
    
    switch (publication.attachmentsArray.count) {
        case 0:
            
            break;
        case 1:
            itemSize.height = [[UIScreen mainScreen] bounds].size.height*2/3;
            if (attachment.width > self.view.frame.size.width)
            {
                itemSize.width = self.view.frame.size.width;
                itemSize.height = attachment.height * itemSize.width / attachment.width-1;
            }
            break;
            
        default:
            itemSize.height = [[UIScreen mainScreen] bounds].size.height/3;
            break;
    }
    
    if (itemSize.height > attachment.height)
    {
        itemSize.height = attachment.height-1;
    }
        
    itemSize.width = attachment.width * itemSize.height / attachment.height;
    return itemSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView
                        layout:(UICollectionViewLayout *) collectionViewLayout
        insetForSectionAtIndex:(NSInteger) section {
    
    return UIEdgeInsetsMake(0, 0, 0, 5); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *) collectionView
                   layout:(UICollectionViewLayout *) collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
    return 5.0;
}

-(void)collectionView:(IndexedCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Publication *publication = self.feedObjects[collectionView.index];
    Attachment *selectedAttachment = publication.attachmentsArray[indexPath.row];
    
    if (selectedAttachment.attachType == PhotoType)
    {
        NSMutableArray *photosArray = [[NSMutableArray alloc]init];
        for (Attachment *attach in publication.photosArray)
        {
            IDMPhoto *photoForGallery = [IDMPhoto photoWithURL:[NSURL URLWithString:attach.fullUrl]];
            [photosArray addObject:photoForGallery];
        }

        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photosArray];
        [browser setInitialPageIndex:indexPath.item];
        [self presentViewController:browser animated:YES completion:nil];
    }
}

//==================------------------
#pragma mark - TableView
//==================------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.feedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"PublicationCell";
    
    PublicationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Publication *publication = self.feedObjects[indexPath.row];
        
    [cell setCell:publication];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Publication *publication = self.feedObjects[indexPath.row];
    
    float stringHeight = [Helper findHeightForText:[NSString stringWithFormat:@"%@",publication.annotation ] havingWidth:self.view.frame.size.width-22 andFont:[UIFont systemFontOfSize:13]].height + 58;

    stringHeight += [Helper findHeightForText:publication.title havingWidth:self.view.frame.size.width-19 andFont:[UIFont boldSystemFontOfSize:13]].height + 5;
    
    if (publication.hasFullContent) stringHeight += 27;

    if (publication.attachmentsArray.count) stringHeight += [Helper heightForPhotosCollectionsView:publication] + 2;

    if (publication.recommendationsCount > 0) stringHeight += 15+2;

    if (publication.rootCommentCount > 0) stringHeight += 35;
    
    stringHeight += publication.pdfArray.count * 30;
    
    return stringHeight + 10;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedPublication = self.feedObjects[indexPath.row];
    [self performSegueWithIdentifier:@"showPublication" sender:self];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(PublicationTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
}



@end
