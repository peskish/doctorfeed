//
//  CreatePublicationTypes.m
//  doctorFeed
//
//  Created by Artem Peskishev on 28.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "PublicationTypeToCreate.h"

@implementation PublicationTypeToCreate

- (void)import:(NSDictionary *)dict
{
    self.identifier = [dict objectForKey:@"id"];
    self.title = [dict objectForKey:@"title"];
    self.desc = [dict objectForKey:@"description"];
    self.doctorCanCreate = [[dict objectForKey:@"doctorCanCreate"] boolValue];
    self.doctorCanSelect = [[dict objectForKey:@"doctorCanSelect"] boolValue];;
    self.selectOrderIndex = [dict objectForKey:@"selectOrderIndex"];
    self.specialtyRequired = [[dict objectForKey:@"specialtyRequired"] boolValue];
    self.needTitle = [[dict objectForKey:@"needTitle"] boolValue];
    self.needContent = [[dict objectForKey:@"needContent"] boolValue];
    
    self.minTitleLength = [[dict objectForKey:@"minTitleLength"] integerValue];
    self.maxTitleLength = [[dict objectForKey:@"maxTitleLength"] integerValue];
    self.minContentLength = [[dict objectForKey:@"minContentLength"] integerValue];
    self.maxContentLength = [[dict objectForKey:@"maxContentLength"] integerValue];
    
}

@end
