//
//  APIManager.m
//  doctorFeed
//
//  Created by Artem Peskishev on 06.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "APIManager.h"
#import "DFSpecialty.h"

@interface APIManager()
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiClient;
@property NSMutableDictionary *postsDict;
@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSMutableArray *specialtiesList;

@end

@implementation APIManager


//====================----------------------------
#pragma mark - Initialization
//====================----------------------------

+ (APIManager *)sharedManager
{
    static APIManager *__sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[APIManager alloc] init];
    });
    return __sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.apiClient = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:API_BASE]];
        self.apiClient.responseSerializer = [AFJSONResponseSerializer serializer];
        self.specialtiesList = [NSMutableArray new];
    }
    return self;
}

//====================----------------------------
#pragma mark - Login
//====================----------------------------

- (void)loginWithMail:(NSString *)email
                  pwd:(NSString *)pwd
      completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock
{
    NSDictionary *parameters = @{@"login" : email,
                                 @"password" : pwd,
                                 @"applicationId" : APP_ID};
    
    [self.apiClient POST:@"auth" parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
    {
        NSDictionary *loadedAnswer = responseObject;
        if (loadedAnswer[@"token"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:loadedAnswer[@"token"] forKey:@"token"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self.apiClient.requestSerializer setValue:loadedAnswer[@"token"] forHTTPHeaderField:@"token"];
            
            [self.specialtiesList removeAllObjects];
            for (NSDictionary *dict in loadedAnswer[@"specialties"])
            {
                DFSpecialty * specialty = [DFSpecialty new];
                [specialty import:dict];
                [self.specialtiesList addObject:specialty];
            }
        }
        completioBlock(loadedAnswer,nil);
        
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error)
    {
        completioBlock(nil,error);
    }];
    
}

-(NSArray *)getSpecialtyList
{
    return self.specialtiesList;
}

//====================----------------------------
#pragma mark - MainFeed load
//====================----------------------------

- (void)loadFeedFromDate:(NSDate *)startDate
         completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];

    NSDictionary *parameters = @{@"StartDate" : [formatter stringFromDate:startDate],
                                 @"Take" : @"15"};
    
    [self.apiClient GET:@"ribbon" parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        completioBlock(responseObject,nil);
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        completioBlock(nil,error);
    }];
}

//====================----------------------------
#pragma mark - Publication load
//====================----------------------------

- (void)loadPublication:(NSString *)publicationId
        completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock
{
    NSDictionary *parameters = @{@"id" : publicationId};
    
    [self.apiClient GET:@"publications"
             parameters:parameters
                success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
                    completioBlock(responseObject,nil);
                } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
                    completioBlock(nil,error);
                }];
}

-(void)recommendPublication:(NSString *)publicationId
            completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock
{
    NSString *url = [NSString stringWithFormat:@"publications/%@/recommendations", publicationId];
    [self.apiClient GET:url
             parameters:nil
                success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
                    completioBlock(responseObject,nil);
                } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
                    completioBlock(nil,error);
                }];
}

-(void)createPublicationWithTitle:(NSString *)title
                          content:(NSString *)content
                             type:(NSNumber *)type
                      specialtyId:(NSNumber *)specialtyId
                           photos:(NSArray *)photos
                           videos:(NSArray *)videos
                  completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock
{
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    if (title) {
        [parameters setObject:title forKey:@"title"];
    }
    if (content) {
        [parameters setObject:content forKey:@"content"];
    }
    if (type) {
        [parameters setObject:type forKey:@"type"];
    }
    if (specialtyId) {
        [parameters setObject:specialtyId forKey:@"specialtyId"];
    }
    if (photos) {
        [parameters setObject:photos forKey:@"photos"];
    }
    if (videos) {
        [parameters setObject:videos forKey:@"videos"];
    }
    
    [self.apiClient POST:@"publications"
              parameters:parameters
                 success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
                     completioBlock(responseObject,nil);

    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        completioBlock(nil,error);

    }];
}

-(void)uploadPhoto:(Attachment *)attach Progress:(void (^)(float progress))progressBlock CompletionBlock:(void (^)(NSDictionary *answer, NSError *error))completionBlock
{
    NSData *imageData = attach.imageData;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMdd_hhmmss"];
    NSString *imageName = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
    
    NSString *urlString = [API_BASE stringByAppendingString:@"/Attachment/UploadFiles"];
    
    
    // 1. Create `AFHTTPRequestSerializer` which will create your request.
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    // 2. Create an `NSMutableURLRequest`.
    NSMutableURLRequest *request =
    [serializer multipartFormRequestWithMethod:@"POST" URLString:urlString
                                    parameters:nil
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                         [formData appendPartWithFileData:imageData
                                                     name:@"file"
                                                 fileName:[NSString stringWithFormat:@"%@.jpg", imageName]
                                                 mimeType:@"image/jpeg"];
                     }];
    
    [request setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"token"] forHTTPHeaderField:@"token"];
    
    // 3. Create and use `AFHTTPRequestOperationManager` to create an `AFHTTPRequestOperation` from the `NSMutableURLRequest` that we just created.
    AFHTTPRequestOperation *operation =
    [self.apiClient HTTPRequestOperationWithRequest:request
                                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                         NSLog(@"Success %@", responseObject);
                                         completionBlock(responseObject,nil);
                                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         NSLog(@"Failure %@", error.description);
                                         completionBlock(nil,error);
                                     }];
    
    // 4. Set the progress block of the operation.
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
        progressBlock((float)totalBytesWritten/(float)totalBytesExpectedToWrite);
    }];
    
    // 5. Begin!
    [operation start];
}


//====================----------------------------
#pragma mark - Comments load
//====================----------------------------

- (void)loadCommentsForPublication:(NSString *)publicationId
                           endDate:(NSDate *)endDate
                             count:(NSString *)count
                   completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock
{
    NSMutableDictionary *parameters = [NSMutableDictionary new];

    if (endDate) {
        NSDateFormatter *formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [parameters setObject:[formatter stringFromDate:endDate] forKey:@"endDate"];
    }
    if (count) {
        [parameters setObject:count forKey:@"Take"];
    }
    
    NSString *url = [@"publications" stringByAppendingFormat:@"/%@/%@", publicationId, @"comments"];

    [self.apiClient GET:url
             parameters:parameters
                success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
                    completioBlock(responseObject,nil);
                } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
                    completioBlock(nil,error);
                }];
}

@end
