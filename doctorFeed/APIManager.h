//
//  APIManager.h
//  doctorFeed
//
//  Created by Artem Peskishev on 06.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "Attachment.h"

#define API_BASE @"http://testapi.doktornarabote.ru/api/v5"
#define APP_ID @"83d0cfd3-f259-4aef-9007-3e84553438cf"

@interface APIManager : NSObject

//singletone
+ (APIManager *)sharedManager;

//====================----------------------------
//Login
//====================----------------------------
- (void)loginWithMail:(NSString *)email
                  pwd:(NSString *)pwd
      completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock;

-(NSArray *)getSpecialtyList;
//====================----------------------------
//MainFeed
//====================----------------------------
- (void)loadFeedFromDate:(NSDate *)startDate
         completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock;

//====================----------------------------
//Publication load
//====================----------------------------

- (void)loadPublication:(NSString *)publicationId
        completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock;

- (void)createPublicationWithTitle:(NSString *)title
                           content:(NSString *)content
                              type:(NSNumber *)type
                       specialtyId:(NSNumber *)specialtyId
                            photos:(NSArray *)photos
                            videos:(NSArray *)videos
                   completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock;

-(void)uploadPhoto:(Attachment *)attach Progress:(void (^)(float progress))progressBlock CompletionBlock:(void (^)(NSDictionary *answer, NSError *error))completionBlock;


//====================----------------------------
//Comments load
//====================----------------------------

- (void)loadCommentsForPublication:(NSString *)publicationId
                           endDate:(NSDate *)endDate
                             count:(NSString *)count
                   completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock;

-(void)recommendPublication:(NSString *)publicationId
            completionBlock:(void (^)(NSDictionary *answer, NSError *error))completioBlock;


@end
