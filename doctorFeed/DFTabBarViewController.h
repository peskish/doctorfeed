//
//  TabBarViewController.h
//  doctorFeed
//
//  Created by Artem Peskishev on 28.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Doctor.h"

@interface DFTabBarViewController : UITabBarController <UITabBarControllerDelegate>

@property (nonatomic, strong) NSArray * publicationTypes;
@property (nonatomic, strong) NSArray * specialtiesArray;
@property (nonatomic, strong) Doctor *userDoctor;

@end
