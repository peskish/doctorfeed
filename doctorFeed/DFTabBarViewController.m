//
//  TabBarViewController.m
//  doctorFeed
//
//  Created by Artem Peskishev on 28.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "DFTabBarViewController.h"
#import "ChooseCreatePublicationTypeViewController.h"
#import "MainFeedViewController.h"
#import "TLTransitionAnimator.h"
#import "ChooseCreatePublicationTypeViewController.h"
#import "ARTransitionAnimator.h"

@interface DFTabBarViewController () <UIViewControllerTransitioningDelegate>
@property (nonatomic, strong) ARTransitionAnimator *transitionAnimator;

@end

@implementation DFTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    self.view.backgroundColor = [UIColor clearColor];
    self.modalPresentationStyle = UIModalPresentationFullScreen;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[ChooseCreatePublicationTypeViewController class]])
    {
        
    }
}

- (BOOL) tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if([viewController isKindOfClass:[ChooseCreatePublicationTypeViewController class]]){

//        [self performSegueWithIdentifier:@"createPublication" sender:self];
        [self openCreatePublicationView];
        return NO;
    }else{
        return YES;
    }
    
}

-(void)openCreatePublicationView
{
    self.transitionAnimator = [[ARTransitionAnimator alloc] init];
    self.transitionAnimator.transitionDuration = 0.3;
    self.transitionAnimator.transitionStyle = ARTransitionStyleRightToLeft;
    
    UINavigationController *navController =  (UINavigationController *)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"chooseCreatePublicationType"];
    ChooseCreatePublicationTypeViewController *viewController = (ChooseCreatePublicationTypeViewController *)[navController.viewControllers objectAtIndex:0];
    viewController.publicationTypes = self.publicationTypes;
    navController.modalPresentationStyle = UIModalPresentationCustom;
    navController.transitioningDelegate = self.transitionAnimator;
    [self presentViewController:navController animated:YES completion:nil];
}


@end
