//
//  PDF.m
//  doctorFeed
//
//  Created by Artem Peskishev on 20.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "PDF.h"

@implementation PDF

- (void)createWithDict:(NSDictionary *)dict
{
    self.url = dict[@"url"];
    self.title = dict[@"title"];
    self.size = [dict[@"size"] integerValue];
}

@end
