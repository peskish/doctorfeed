//
//  Photo.h
//  doctorFeed
//
//  Created by Artem Peskishev on 07.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Attachment.h"

@interface Photo : Attachment

-(void)createWithDictionary:(NSDictionary *)dict;

@end
