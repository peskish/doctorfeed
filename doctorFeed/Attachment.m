//
//  Attachment.m
//  doctorFeed
//
//  Created by Artem Peskishev on 19.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "Attachment.h"
#import "Photo.h"
#import "Video.h"

@implementation Attachment

-(void)createWithDictionary:(NSDictionary *)dict
{
    self.previewUrl = dict[@"previewUrl"];
    self.fullUrl = dict[@"url"];
}

@end
