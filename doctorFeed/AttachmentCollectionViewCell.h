//
//  PhotoCollectionViewCell.h
//  doctorFeed
//
//  Created by Artem Peskishev on 09.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Attachment.h"

@interface AttachmentCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UIButton *playVideoButton;

- (void)setCellWithAttachment:(Attachment *)attachment;

@end
