//
//  CreatePublicationViewController.m
//  doctorFeed
//
//  Created by Artem Peskishev on 30.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "CreatePublicationViewController.h"
#import "DFTabBarViewController.h"
#import "DFSpecialty.h"
#import "DFPhotoToSendCollectionViewCell.h"
#import <CTAssetsPickerController/CTAssetCheckmark.h>
#import <CTAssetsPickerController/CTAssetCollectionViewCell.h>
#import <CTAssetsPickerController/CTAssetSelectionLabel.h>
#import <CTAssetsPickerController/CTAssetsGridSelectedView.h>
#import <CTAssetsPickerController/CTAssetsGridView.h>
#import <CTAssetsPickerController/CTAssetsGridViewCell.h>
#import <CTAssetsPickerController/CTAssetsGridViewFooter.h>
#import <CTAssetsPickerController/CTAssetsPageView.h>
#import "MPTextView.h"
#import "STAlertView.h"


@interface CreatePublicationViewController () <UIAlertViewDelegate, UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *specialtyTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *specialtyTextFieldHeight;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTextFieldHeight;
@property (weak, nonatomic) IBOutlet MPTextView *contentTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentTextViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UIPickerView *specialtyPicker;
@property (strong, nonatomic) NSArray *specialtiesArray;
@property (strong, nonatomic) DFSpecialty *choosedSpecialty;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *uploadView;
@property (weak, nonatomic) IBOutlet UICollectionView *photosCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photosCollectionViewHeight;
@property (nonatomic, strong) NSMutableArray *photosArray;
@property (nonatomic, strong) NSMutableArray *videosArray;
@property (nonatomic, strong) STAlertView *alertView;
@property (weak, nonatomic) IBOutlet UILabel *mailInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryInfoLabel;
@end

@implementation CreatePublicationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.photosArray = [NSMutableArray new];
    self.videosArray = [NSMutableArray new];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removePhoto:)
                                                 name:@"removePhoto"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removeVideo:)
                                                 name:@"removeVideo"
                                               object:nil];
    
    self.scrollViewWidthConstraint.constant = self.view.frame.size.width;
    
    [self.view layoutIfNeeded];
    
    self.specialtyTextField.rightViewMode = UITextFieldViewModeAlways;
    self.specialtyTextField.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dropdown.png"]];
    
    self.sendButton.layer.cornerRadius = 3;
    self.contentTextView.layer.cornerRadius = 3;
    self.uploadView.layer.cornerRadius = 5;
    
    
    self.specialtiesArray = [[APIManager sharedManager] getSpecialtyList];
    
    if (!self.pubType.needTitle) self.titleTextFieldHeight.constant = 0;
    if (!self.pubType.specialtyRequired) self.specialtyTextFieldHeight.constant = 0;
    if (!self.pubType.needContent) self.contentTextViewHeight.constant = 0;
    self.mainScrollView.delegate = self;
    
    [self.categoryInfoLabel setText:self.pubType.desc];
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.contentSize.width, CGRectGetMaxY(self.mailInfoLabel.frame));
    
    self.photosCollectionViewHeight.constant = 0;
    
    self.contentTextView.placeholderText = @"Изложите свои мысли лаконично и по существу, перед отправкой проверьте орфографию";
}

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textView
{
    if (textView == self.specialtyTextField)
    {
        self.specialtyPicker.hidden = NO;
        [self.view endEditing:YES];
        return NO;
    }
    
    return YES;
}

//==============================-------------------
#pragma mark - Buttons Action
//==============================-------------------

- (void)removePhoto:(NSNotification *)notification
{
    Attachment *attachmentToRemove = notification.object;
    [self.photosArray removeObject:attachmentToRemove];
    [self.photosCollectionView reloadData];
    
    if (!self.photosArray.count)
    {
        self.photosCollectionViewHeight.constant = 0;
    }
}

- (void)removeVideo:(NSNotification *)notification
{
    NSString *videoToRemove = notification.object;
    [self.videosArray removeObject:videoToRemove];
    [self.photosCollectionView reloadData];
}

- (IBAction)createPublicationButtonTapped:(id)sender
{
    [SVProgressHUD showWithStatus:@"Создаю публикацию"];
    
    NSMutableArray *photos = [NSMutableArray new];
    
    for (Attachment *attachment in self.photosArray)
    {
        [photos addObject:attachment.fileId];
    }
    
    [[APIManager sharedManager]createPublicationWithTitle:self.titleTextField.text content:self.contentTextView.text type:self.pubType.identifier specialtyId:self.choosedSpecialty.identifier photos:photos videos:self.videosArray completionBlock:^(NSDictionary *answer, NSError *error) {
        
        if (!answer[@"errorCode"]) {
            
            [SVProgressHUD dismiss];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [SVProgressHUD showErrorWithStatus:answer[@"errorMessage"]];
        }
    }];
}
- (IBAction)addImageButtonTapped:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Выберите источник"
                                                       message:nil
                                                      delegate:self
                                             cancelButtonTitle:@"Сделать фото"
                                             otherButtonTitles:@"Выбрать из галереи", nil];
    [alertView show];
}

- (IBAction)addVideoButtonTapped:(id)sender {
    
    self.alertView = [[STAlertView alloc] initWithTitle:NSLocalizedString(@"Добавление видео", @"Добавление видео")
                                                message:@"Вставьте ссылку на видео в поле ниже"
                                          textFieldHint:@"URL"
                                         textFieldValue:nil
                                      cancelButtonTitle:@"Отмена"
                                       otherButtonTitle:@"Добавить"
                                      cancelButtonBlock:^{

                                      } otherButtonBlock:^(NSString * result){
                                          if (!self.videosArray) {
                                              self.videosArray = [[NSMutableArray alloc]init];
                                          }
                                          
                                          NSString *videoURL = [[self.alertView.alertView textFieldAtIndex:0] text];
                                          if ([videoURL rangeOfString:@"youtu"].location == NSNotFound)
                                          {
                                              UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Ошибка", @"Ошибка")
                                                                                              message:NSLocalizedString(@"Вы должны указать ссылку на YouTube видео",@"Вы должны указать ссылку на YouTube видео")
                                                                                             delegate:nil
                                                                                    cancelButtonTitle:@"OK"
                                                                                    otherButtonTitles: nil];
                                              [alert show];
                                          } else {
                                              [self.videosArray addObject:videoURL];
                                              int rowsCount = ceil(((self.photosArray.count + self.videosArray.count) * 75) / self.photosCollectionView.frame.size.width);
                                              self.photosCollectionViewHeight.constant = rowsCount * 75;
                                              
                                              self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.contentSize.width, CGRectGetMaxY(self.mailInfoLabel.frame));
                                              [self.view layoutSubviews];
                                              [self.photosCollectionView reloadData];
                                          }
                                      }];
    
    //You can make any customization to the native UIAlertView
    self.alertView.alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [self.alertView show];
}

//==============================--------------------------
#pragma mark - ImagePicker
//==============================--------------------------

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            //camera
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
            break;
        }
        case 1: {
            //gallery
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status){
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // init picker
                    CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
                    picker.showsCancelButton = YES;
                    picker.showsEmptyAlbums = NO;
                    picker.delegate = self;
                    picker.defaultAssetCollection = PHAssetCollectionSubtypeSmartAlbumUserLibrary;
                    picker.showsSelectionIndex = YES;
                    [self setupImagePickerView];
                    
                    // present picker
                    [self presentViewController:picker animated:YES completion:nil];
                });
            }];
            break;
        }
        default:
            break;
    }
}

- (void)setupImagePickerView
{
    UINavigationBar *navBar = [UINavigationBar appearanceWhenContainedIn:[CTAssetsPickerController class], nil];
    [navBar setBarTintColor:[UIColor blackColor]];
    navBar.tintColor = [UIColor whiteColor];
    navBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    // albums view
    UITableView *assetCollectionView = [UITableView appearanceWhenContainedIn:[CTAssetsPickerController class], nil];
    assetCollectionView.backgroundColor = [UIColor blackColor];
    
    // grid view
    CTAssetsGridView *assetsGridView = [CTAssetsGridView appearance];
    assetsGridView.gridBackgroundColor = [UIColor blackColor];
    
    CTAssetsGridViewFooter *assetsGridViewFooter = [CTAssetsGridViewFooter appearance];
    assetsGridViewFooter.textColor = [UIColor whiteColor];
    
    CTAssetsGridSelectedView *assetsGridSelectedView = [CTAssetsGridSelectedView appearance];
    assetsGridSelectedView.tintColor = [UIColor colorWithRed:0.06 green:0.51 blue:1 alpha:1];
    assetsGridSelectedView.borderWidth = 3.0;
    
    CTAssetSelectionLabel *assetSelectionLabel = [CTAssetSelectionLabel appearance];
    assetSelectionLabel.borderWidth = 1.0;
    assetSelectionLabel.borderColor = [UIColor colorWithRed:0.06 green:0.51 blue:1 alpha:1];
    [assetSelectionLabel setSize:CGSizeMake(20.0, 20.0)];
    [assetSelectionLabel setMargin:3.0 forVerticalEdge:NSLayoutAttributeRight horizontalEdge:NSLayoutAttributeTop];
    [assetSelectionLabel setTextAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:13],
                                             NSForegroundColorAttributeName : [UIColor whiteColor],
                                             NSBackgroundColorAttributeName : [UIColor colorWithRed:0.06 green:0.51 blue:1 alpha:1]}];
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        UIImage *imageToUpload = [Helper scaleAndRotateImage:chosenImage];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyyMMdd_hhmmss"];
        NSString *imageName = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
        Attachment *attachment = [Attachment new];
        attachment.filename = imageName;
        attachment.imageData = UIImageJPEGRepresentation(imageToUpload, 0);
        [self.photosArray addObject:attachment];
        [self uploadImage:attachment];
    }];
}

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    PHImageManager *manager = [PHImageManager defaultManager];
    
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    options.synchronous = NO;
    options.networkAccessAllowed = YES;
    
    __block int counter = 0;
    __block float totalProgress = 0;
    
    self.view.userInteractionEnabled = NO;
    [SVProgressHUD showProgress:0 status:@"0"];
    for (PHAsset *asset in assets)
    {
        Attachment *attachment = [Attachment new];
        attachment.filename = [asset valueForKey:@"filename"];
        __block float currentProgress = 0;
        options.progressHandler = ^(double progress, NSError *error, BOOL *stop, NSDictionary *info) {
            totalProgress = totalProgress - currentProgress + progress;
            currentProgress = progress;
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showProgress:totalProgress/3 status:[NSString stringWithFormat:@"Обработка"]];
            });
        };
        
        [manager requestImageDataForAsset:asset
                                  options:options
                            resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info)
         {
             if (imageData)
             {
                 //                 attachment.imageData = imageData;
                 
                 UIImage *rotatedImage = [UIImage imageWithData:imageData];
                 rotatedImage = [Helper scaleAndRotateImage:rotatedImage];
                 attachment.imageData = UIImageJPEGRepresentation(rotatedImage, 0);
                 
                 
                 [self.photosArray addObject:attachment];
                 counter++;
                 [self uploadImage:attachment];

                 if (counter == assets.count)
                 {
                     self.view.userInteractionEnabled = YES;
                     
                     [SVProgressHUD dismiss];
                     
                 }
             }
         }];
    }
}

- (void)uploadImage:(Attachment *)attachment
{
    [self.photosCollectionView reloadData];
    int rowsCount = ceil(((self.photosArray.count + self.videosArray.count) * 75) / self.photosCollectionView.frame.size.width);
    self.photosCollectionViewHeight.constant = rowsCount * 75;
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.contentSize.width, CGRectGetMaxY(self.mailInfoLabel.frame));
    [self.view layoutSubviews];
    
        
    [[APIManager sharedManager]uploadPhoto:attachment Progress:^(float progress) {
        attachment.uploadProgress = progress;
        if ([self.photosArray containsObject:attachment])
        {
            DFPhotoToSendCollectionViewCell *cell = (DFPhotoToSendCollectionViewCell *)[self.photosCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:[self.photosArray indexOfObject:attachment] inSection:0]];
            [cell.progressView setProgress:progress];
            if (progress != 1)
            {
                cell.progressView.hidden = NO;
            } else {
                cell.progressView.hidden = YES;
            }
        }
        
    } CompletionBlock:^(NSDictionary *answer, NSError *error) {
        if (answer[@"items"][0][@"fileId"])
        {
            attachment.fileId = answer[@"items"][0][@"fileId"];
        }
    }];

}

//==============================-------------------
#pragma mark - PickerView DataSource
//==============================-------------------

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.specialtiesArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    DFSpecialty *specialty = self.specialtiesArray[row];
    return specialty.name;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    DFSpecialty *specialty = self.specialtiesArray[row];
    self.choosedSpecialty = specialty;
    [self.specialtyTextField setText:specialty.name];
}

//==--------------------------------------------
#pragma mark - CollectionView DELEGATE
//==--------------------------------------------

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    NSInteger attachmentsCount = self.photosArray.count + self.videosArray.count;
    return attachmentsCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DFPhotoToSendCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"photoToSend" forIndexPath:indexPath];
    if (indexPath.item < self.photosArray.count)
    {
        [cell setCellWithAttachment:self.photosArray[indexPath.item]];
    } else {
        [cell setCellWithVideo:(NSString *)self.videosArray[indexPath.item - self.photosArray.count]];
    }
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeZero;
    size.width = 75;
    size.height = 75;
    return size;
}

//=======================================================
#pragma mark - KEYBOARD
//=======================================================

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    self.scrollViewBottomConstraint.constant = rect.size.height - self.tabBarController.tabBar.frame.size.height;
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.contentSize.width, CGRectGetMaxY(self.mailInfoLabel.frame));
    [self.view layoutSubviews];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    self.scrollViewBottomConstraint.constant = 0;
    [self.view layoutSubviews];
}


@end
