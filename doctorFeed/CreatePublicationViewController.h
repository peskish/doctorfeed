//
//  CreatePublicationViewController.h
//  doctorFeed
//
//  Created by Artem Peskishev on 30.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PublicationTypeToCreate.h"
#import <CTAssetsPickerController/CTAssetsPickerController.h>

@interface CreatePublicationViewController : UIViewController <CTAssetsPickerControllerDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIScrollViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) PublicationTypeToCreate *pubType;

@end
