//
//  Doctor.m
//  doctorFeed
//
//  Created by Artem Peskishev on 06.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "Doctor.h"

@implementation Doctor

-(id)initPublicationAuthorWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.avatarUrl = dict[@"avatar"];
        self.shortName = dict[@"shortName"];
        self.specialty = dict[@"specialty"];
    }
    return self;
}

-(id)initUserDoctorWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.avatarUrl = dict[@"smallAvatarLink"];
        self.shortName = dict[@"shortName"];
    }
    return self;
}
@end
