//
//  Doctor.h
//  doctorFeed
//
//  Created by Artem Peskishev on 06.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Doctor : NSObject

@property NSString *avatarUrl;
@property NSString *shortName;
@property NSString *specialty;

@property NSString *largeAvatarUrl;

-(id)initPublicationAuthorWithDict:(NSDictionary *)dict;

-(id)initUserDoctorWithDict:(NSDictionary *)dict;

@end
