//
//  CreatePublicationViewController.m
//  doctorFeed
//
//  Created by Artem Peskishev on 28.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "ChooseCreatePublicationTypeViewController.h"
#import "CreatePublicationTableViewCell.h"
#import "PublicationTypeToCreate.h"
#import "CreatePublicationViewController.h"


@interface ChooseCreatePublicationTypeViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewWidthConstraint;
@property NSArray *dataArray;
@property (nonatomic, strong) PublicationTypeToCreate *selectedPubType;
@end

@implementation ChooseCreatePublicationTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.contentViewWidthConstraint.constant = self.view.frame.size.width;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    NSMutableArray *array = [NSMutableArray new];
    for (PublicationTypeToCreate *pubType in self.publicationTypes)
    {
        if (pubType.doctorCanCreate)
        {
            [array addObject:pubType];
        }
    }
    
    self.dataArray = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber *first = [(PublicationTypeToCreate*)a selectOrderIndex];
        NSNumber *second = [(PublicationTypeToCreate*)b selectOrderIndex];
        return [first compare:second];
    }];
    
    [self.tableView reloadData];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showCreatePublicationContent"])
    {
        CreatePublicationViewController *vc = segue.destinationViewController;
        vc.pubType = self.selectedPubType;
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (IBAction)backButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//==================------------------
#pragma mark - TableView
//==================------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"createPublicationCell";
    
    CreatePublicationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    PublicationTypeToCreate *publicationType = self.dataArray[indexPath.row];
    
    [cell setCell:publicationType];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedPubType = self.dataArray[indexPath.row];
    [self performSegueWithIdentifier:@"showCreatePublicationContent" sender:self];
}






@end
