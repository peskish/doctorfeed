//
//  PublicationCommentTableViewCell.h
//  doctorFeed
//
//  Created by Artem Peskishev on 23.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Doctor.h"
#import "IndexedCollectionView.h"

@interface PublicationCommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *commentViewBackground;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *specialtyLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdAtLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *replyButton;

@property (weak, nonatomic) IBOutlet UIView *whiteBackgroundView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *whiteBackgroundBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *moreButtonConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photosCollectionViewHeight;
@property NSInteger commentIndex;
@property (weak, nonatomic) IBOutlet IndexedCollectionView *photosCollectionView;

-(void)setCell:(Publication *)comment;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;

@end
