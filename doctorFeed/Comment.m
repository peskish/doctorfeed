//
//  Comment.m
//  doctorFeed
//
//  Created by Artem Peskishev on 25.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "Comment.h"
#import "Photo.h"

@implementation Comment

-(void)createWithDictionary:(NSDictionary *)dict
{
    [super createWithDictionary:dict];
    
    self.annotation = dict[@"shortContent"];

    self.rootCommentCount = [dict[@"repliesCount"]integerValue];
    
    self.showFullComment = NO;
}

@end
