//
//  DFPDFViewerViewController.m
//  doctorFeed
//
//  Created by Artem Peskishev on 29.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "DFPDFViewerViewController.h"

@interface DFPDFViewerViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *savedFilePath;
@property (strong, nonatomic) UIDocumentInteractionController *docController ;
@end

@implementation DFPDFViewerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webView.delegate = self;
    NSURL *targetURL = [NSURL URLWithString:self.pdfURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [self.webView loadRequest:request];
    self.webView.hidden = YES;
    [SVProgressHUD showWithStatus:@"Загружаю PDF"];

}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
    [self removePDFFromLocalDirectory];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.webView.hidden = NO;
    [SVProgressHUD dismiss];
    [self savePDFInLocalDirectory];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"ibooks://"]])
    {
        [self showOpenInButton];
    }
}

- (void)showOpenInButton
{
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(openPDFIn:)];
    self.navigationItem.rightBarButtonItem = anotherButton;
}

-(void)openPDFIn:(id)sender
{
    NSURL *pdfURL = [NSURL fileURLWithPath:self.savedFilePath];
    self.docController = [UIDocumentInteractionController interactionControllerWithURL:pdfURL];
    
    //present a drop down list of the apps that support the file type, click an item in the list will open that app while passing in the file.
    [self.docController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
}

//==========================-------------
#pragma mark - PDF Local saver
//==========================-------------

//Так как прямые УРЛы кривые, вот такой вот костыль для сейва в ibooks.
- (void)savePDFInLocalDirectory
{
    NSData *pdfFile = [NSData dataWithContentsOfURL:self.webView.request.URL];
    NSString *docsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [[NSString alloc] initWithString: [docsPath stringByAppendingPathComponent:@"randomPDFName.pdf"]];
    [pdfFile writeToFile:filePath atomically:YES];
    self.savedFilePath = filePath;
}

- (void)removePDFFromLocalDirectory
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:@"randomPDFName.pdf"];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success)
    {
        NSLog(@"Successfully Deleted");
    }
}

@end
