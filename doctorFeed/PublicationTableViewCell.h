//
//  PublicationTableViewCell.h
//  doctorFeed
//
//  Created by Artem Peskishev on 06.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Publication.h"
#import "Doctor.h"
#import "IndexedCollectionView.h"

@interface PublicationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *publicationView;

@property (weak, nonatomic) IBOutlet UIImageView *doctorAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *doctorSpecialtyLabel;
@property (weak, nonatomic) IBOutlet UILabel *doctorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UILabel *publicationHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *publicationContentLabel;

@property (weak, nonatomic) IBOutlet UIView *commentsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentsHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *commentsCountLabel;

@property (weak, nonatomic) IBOutlet IndexedCollectionView *photosCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photosCollectionViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *recommendationsCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *moreButtonHeight;

@property (weak, nonatomic) IBOutlet UIView *pdfsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pdfsViewHeight;

- (void)setCell:(Publication *)publication;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;
@end
