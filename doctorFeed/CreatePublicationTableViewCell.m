//
//  CreatePublicationTableViewCell.m
//  doctorFeed
//
//  Created by Artem Peskishev on 28.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "CreatePublicationTableViewCell.h"

@implementation CreatePublicationTableViewCell

- (void)awakeFromNib {

}

-(void)setCell:(PublicationTypeToCreate *)pubType
{
    self.backView.layer.cornerRadius = self.backView.frame.size.height/2;
    self.backView.clipsToBounds = YES;
    [self.publicationTypeNameLabel setText:pubType.title];
}

@end
