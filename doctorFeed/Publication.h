//
//  Publication.h
//  doctorFeed
//
//  Created by Artem Peskishev on 19.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Doctor.h"

@interface Publication : NSObject

@property (nonatomic, strong) NSString *annotation;         //Короткое содержание
@property (nonatomic, strong) NSString *content;            //Полное содержание
@property (nonatomic, strong) Doctor *author;               //Автор публикации
@property (nonatomic)         BOOL canRecommend;            //Может ли доктор оставить рекомендацию
@property (nonatomic)         BOOL recommendedByDoctor;     //Рекомендовал ли он уже эту публикацию
@property (nonatomic)         NSInteger commentsCount;      //Количество комментариев
@property (nonatomic, strong) NSDate *createdAt;            //Когда публикация создана
@property (nonatomic)         BOOL hasFullContent;          //Обрезана публикация или нет
@property (nonatomic, strong) NSString *publicationID;      //Айди публикации на сервере
@property (nonatomic)         NSInteger rootCommentCount;   //Количество комментариев
@property (nonatomic, strong) NSArray *photosArray;         //Фото прикрепленные к публикации
@property (nonatomic)         NSString* type;               //Тип
@property (nonatomic, strong) NSArray *videosArray;         //Видео прикрепленные к публикации
@property (nonatomic, strong) NSArray *attachmentsArray;    //Фото+видео
@property (nonatomic, strong) NSArray *pdfArray;
//FeedPublication
@property (nonatomic)         NSInteger recommendationsCount; //Количество рекомендаций
@property (nonatomic, strong) NSString *title;                //Заголовок

//Comment
@property (nonatomic) BOOL showFullComment; //показан ли комментарий полностью

- (void)createWithDictionary:(NSDictionary *)dict;

@end
