//
//  Publication.m
//  doctorFeed
//
//  Created by Artem Peskishev on 19.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "Publication.h"

@implementation Publication

- (void)createWithDictionary:(NSDictionary *)dict
{
    NSMutableArray *photosArray = [[NSMutableArray alloc]init];
    for (NSDictionary *photoDict in dict[@"photos"])
    {
        Attachment *photo = [AttachmentFactory initWithDictionary:photoDict attachmentType:PhotoType];
        [photosArray addObject:photo];
    }
    
    NSMutableArray *videosArray = [[NSMutableArray alloc] init];
    for (NSDictionary *videoDict in dict[@"videos"])
    {
        Attachment *video = [AttachmentFactory initWithDictionary:videoDict attachmentType:VideoType];
        [videosArray addObject:video];
    }
    
    NSMutableArray *pdfArray = [[NSMutableArray alloc]init];
    for (NSDictionary *pdfDict in dict[@"pdfs"])
    {
        PDF *pdf = [PDF new];
        [pdf createWithDict:pdfDict];
        [pdfArray addObject:pdf];
    }
    
    self.pdfArray = [[NSArray alloc] initWithArray:pdfArray];
    
    self.attachmentsArray = [photosArray arrayByAddingObjectsFromArray:videosArray];
    
    self.photosArray = [[NSArray alloc]initWithArray:photosArray];
    self.videosArray = [[NSArray alloc] initWithArray:videosArray];
    
    self.author = [[Doctor alloc]initPublicationAuthorWithDict:dict[@"author"]];
    self.canRecommend = [dict[@"canRecommend"] boolValue];
    self.content = dict[@"content"];
    self.createdAt = [Helper dateFromString:dict[@"createdAt"]];
    self.hasFullContent = [dict[@"hasFullContent"] boolValue];
    self.publicationID = dict[@"id"];
    self.type = dict[@"type"];
}

@end
