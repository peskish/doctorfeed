//
//  PublicationCommentTableViewCell.m
//  doctorFeed
//
//  Created by Artem Peskishev on 23.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "PublicationCommentTableViewCell.h"
#import <UIImageView+WebCache.h>
#import "NSAttributedString+DDHTML.h"

@implementation PublicationCommentTableViewCell

- (void)awakeFromNib {
    self.commentViewBackground.layer.cornerRadius = 4;
    self.avatarImageView.layer.cornerRadius = 4;
    self.replyButton.layer.cornerRadius = 4;
}

-(void)setCell:(Publication *)comment
{
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:comment.author.avatarUrl]];
    [self.specialtyLabel setText:comment.author.specialty];
    [self.nameLabel setText:comment.author.shortName];
    [self.createdAtLabel setText:[Helper formatDate:comment.createdAt]];
   
    NSAttributedString *attributedString = [NSAttributedString new];
    
    if (comment.showFullComment)
    {
        attributedString = [NSAttributedString attributedStringFromHTML:comment.content];
    } else {
        attributedString = [NSAttributedString attributedStringFromHTML:comment.annotation];
    }
    NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc]initWithAttributedString:attributedString];
    [mutableString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(0, mutableString.length)];
    
    [self.contentLabel setAttributedText:mutableString];
    
    
    if (comment.photosArray.count)
    {
        self.photosCollectionViewHeight.constant = [Helper heightForPhotosCollectionsView:comment];
    }else {
        self.photosCollectionViewHeight.constant = 0;
    }
    
    if (comment.hasFullContent)
    {
        self.moreButtonConstraint.constant = 25;
    } else {
        self.moreButtonConstraint.constant = 0;
    }
}

- (IBAction)showMoreButtonTapped:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"showFullComment" object:@(self.commentIndex)];
}


- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath
{
    self.photosCollectionView.dataSource = dataSourceDelegate;
    self.photosCollectionView.delegate = dataSourceDelegate;
    self.photosCollectionView.index = indexPath.row;
    
    [self.photosCollectionView reloadData];
    
    [self updateConstraints];
}

@end
