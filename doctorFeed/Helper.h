//
//  Helper.h
//  doctorFeed
//
//  Created by Artem Peskishev on 06.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Publication.h"

@interface Helper : NSObject

+(NSDate *)dateFromString:(NSString *)stringDate;
+(CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font;
+(NSString *)format:(NSInteger)daysCount forWords:(NSArray *)words;
+(NSString *)formatDate:(NSDate *)aDate;
+(CGFloat)heightForPhotosCollectionsView:(Publication *)publication;
+ (NSString *)extractYoutubeID:(NSString *)youtubeURL;
+ (UIImage *)scaleAndRotateImage:(UIImage *) image;
@end
