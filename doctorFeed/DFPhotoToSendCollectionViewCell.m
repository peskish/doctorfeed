//
//  DFPhotoToSendCollectionViewCell.m
//  doctorFeed
//
//  Created by Artem Peskishev on 13.05.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "DFPhotoToSendCollectionViewCell.h"
#import <UIImage+AFNetworking.h>

@implementation DFPhotoToSendCollectionViewCell

-(void)setCellWithAttachment:(Attachment *)attachment
{
    self.attachment = attachment;
    self.photoImageView.image = [UIImage imageWithData:attachment.imageData];
}

- (void)setCellWithVideo:(NSString *)videoURL
{
    self.attachment = nil;
    self.videoUrl = videoURL;
    NSString *videoId = [Helper extractYoutubeID:videoURL];
    NSURL *youtubeURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",videoId]];

    [self.photoImageView sd_setImageWithURL:youtubeURL placeholderImage:[UIImage imageNamed:@"video_placeholder"]];
}

- (IBAction)deletePhotoButtonClicked:(id)sender {
    if (self.attachment)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"removePhoto" object:self.attachment];
    } else {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"removeVideo" object:self.videoUrl];
    }
}

@end
