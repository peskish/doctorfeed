//
//  DFPDFViewerViewController.h
//  doctorFeed
//
//  Created by Artem Peskishev on 29.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DFPDFViewerViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong) NSString *pdfURL;

@end
