//
//  CreatePublicationViewController.h
//  doctorFeed
//
//  Created by Artem Peskishev on 28.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseCreatePublicationTypeViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property NSArray *publicationTypes;
@end
