//
//  CreatePublicationTypes.h
//  doctorFeed
//
//  Created by Artem Peskishev on 28.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublicationTypeToCreate : NSObject

@property (nonatomic, strong) NSNumber *identifier;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, assign) BOOL doctorCanCreate;
@property (nonatomic, assign) BOOL doctorCanSelect;
@property (nonatomic, strong) NSNumber *selectOrderIndex;
@property (nonatomic, assign) BOOL specialtyRequired;
@property (nonatomic, assign) BOOL needTitle;
@property (nonatomic, assign) BOOL needContent;
@property (nonatomic, assign) NSInteger minTitleLength;
@property (nonatomic, assign) NSInteger maxTitleLength;
@property (nonatomic, assign) NSInteger minContentLength;
@property (nonatomic, assign) NSInteger maxContentLength;

- (void)import:(NSDictionary *)dict;

@end
