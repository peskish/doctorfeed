//
//  PublicationTableViewCell.m
//  doctorFeed
//
//  Created by Artem Peskishev on 06.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "PublicationTableViewCell.h"
#import <UIImageView+WebCache.h>
#import "NSAttributedString+DDHTML.h"
#import <QuartzCore/QuartzCore.h>

@interface PublicationTableViewCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewLeftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewRightConstraint;
@property Publication *publication;
@end

@implementation PublicationTableViewCell

- (void)awakeFromNib
{
    self.publicationView.layer.cornerRadius = 4;
    self.commentsView.layer.cornerRadius = 4;
    self.doctorAvatarImageView.layer.cornerRadius = 4;
}

-(void)setCell:(Publication *)publication
{
    self.publication = publication;
    
    for (UIButton *pdfSubview in self.pdfsView.subviews)
    {
        [pdfSubview removeFromSuperview];
    }
    
    if (publication.hasFullContent)
    {
        self.moreButtonHeight.constant = 25;
    } else {
        self.moreButtonHeight.constant = 0;
    }
    
    [self.doctorAvatarImageView sd_setImageWithURL:[NSURL URLWithString: publication.author.avatarUrl]];
    [self.doctorSpecialtyLabel setText:publication.author.specialty];
    [self.doctorNameLabel setText:publication.author.shortName];
    [self.publicationHeaderLabel setText:publication.title];
    
    [self.dateLabel setText:[Helper formatDate:publication.createdAt]];

    [self.publicationContentLabel setText:publication.annotation];
    
    if (publication.rootCommentCount > 0)
    {
        [self.commentsCountLabel setText:[NSString stringWithFormat:@"%li %@",(long)publication.rootCommentCount,[Helper format:publication.rootCommentCount forWords:@[@"комментарий",@"комментария",@"комментариев"]]]];
        self.commentsHeightConstraint.constant = 35;
    } else {
        self.commentsHeightConstraint.constant = 0;
    }
    
    if (publication.recommendationsCount > 0)
    {
        [self.recommendationsCountLabel setText:[NSString stringWithFormat:@"%li %@ %@",(long)publication.recommendationsCount,[Helper format:publication.recommendationsCount forWords:@[@"врач",@"врача",@"врачей"]],[Helper format:publication.recommendationsCount forWords:@[@"рекомендовал",@"рекомендовали",@"рекомендовали"]]]];
    } else {
        [self.recommendationsCountLabel setText:@""];
    }
    
    self.photosCollectionViewHeightConstraint.constant = 0;

    if (publication.attachmentsArray.count > 0)
    {
        self.photosCollectionViewHeightConstraint.constant = [Helper heightForPhotosCollectionsView:publication];
        self.collectionViewLeftConstraint.constant = -6;
        self.collectionViewRightConstraint.constant = -6;
        
        if (publication.attachmentsArray.count == 1)
        {
            Attachment *attachment = [publication.attachmentsArray firstObject];
            CGFloat width = self.photosCollectionViewHeightConstraint.constant * attachment.width/attachment.height;
            self.collectionViewLeftConstraint.constant = (self.frame.size.width - width)/2 - 6;
            self.collectionViewRightConstraint.constant = self.collectionViewLeftConstraint.constant - 6;
        }
    }
    
    self.pdfsViewHeight.constant = 0;
    
    if (publication.pdfArray.count)
    {
        [self createPDFsForPublication:publication];
    }
}

- (void)createPDFsForPublication:(Publication *)publication
{
    CGFloat spacing = 5;
    
    for (int i = 0 ; i < publication.pdfArray.count ; i++)
    {
        PDF *pdf = publication.pdfArray[i];
        
        UIButton *pdfButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [pdfButton setFrame:CGRectMake(0, 30*i, self.pdfsView.frame.size.width, 30)];
        [pdfButton setTitle:pdf.title forState:UIControlStateNormal];
        [pdfButton setTitleColor:INTERFACE_COLOR forState:UIControlStateNormal];
        pdfButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        pdfButton.contentEdgeInsets = UIEdgeInsetsMake(spacing, spacing, spacing, spacing);
        pdfButton.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);

        pdfButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [pdfButton setImage:[UIImage imageNamed:@"pdfIcon"] forState:UIControlStateNormal];
        pdfButton.tag = i;
        [pdfButton addTarget:self action:@selector(pdfButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.pdfsView addSubview:pdfButton];
    }
    
    self.pdfsViewHeight.constant = 30*publication.pdfArray.count;

}

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath
{
    self.photosCollectionView.dataSource = dataSourceDelegate;
    self.photosCollectionView.delegate = dataSourceDelegate;
    self.photosCollectionView.index = indexPath.row;
    
    [self.photosCollectionView reloadData];
    
    [self updateConstraints];
}

-(void)pdfButtonTapped:(UIButton *)pdfButton
{
    PDF *pdf = self.publication.pdfArray[pdfButton.tag];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openPDF" object:pdf.url];
}

@end
