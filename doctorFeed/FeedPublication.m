//
//  Publication.m
//  doctorFeed
//
//  Created by Artem Peskishev on 05.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "FeedPublication.h"
#import "Attachment.h"
#import "NSAttributedString+DDHTML.h"

@implementation FeedPublication

- (void)createWithDictionary:(NSDictionary *)dict
{
    [super createWithDictionary:dict];
    
    NSAttributedString *attributedString = [NSAttributedString attributedStringFromHTML:dict[@"annotation"]];
    NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc]initWithAttributedString:attributedString];
    [mutableString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(0, mutableString.length)];
    
    
    self.annotation = mutableString.string;
    self.recommendedByDoctor = [dict[@"recommendedByDoctor"]boolValue];
    self.commentsCount = [dict[@"commentCount"] integerValue];
    self.recommendationsCount = [dict[@"recommendationsCount"] integerValue];
    self.rootCommentCount = [dict[@"rootCommentsCount"] integerValue];
    self.title = dict[@"title"];
}

@end
