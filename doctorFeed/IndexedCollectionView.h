//
//  IndexedCollectionView.h
//  doctorFeed
//
//  Created by Artem Peskishev on 09.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndexedCollectionView : UICollectionView

@property NSInteger index;

@end
