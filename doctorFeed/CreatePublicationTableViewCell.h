//
//  CreatePublicationTableViewCell.h
//  doctorFeed
//
//  Created by Artem Peskishev on 28.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PublicationTypeToCreate.h"

@interface CreatePublicationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *publicationTypeNameLabel;

-(void)setCell:(PublicationTypeToCreate *)pubType;

@end
