//
//  Attachment.h
//  doctorFeed
//
//  Created by Artem Peskishev on 19.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Attachment : NSObject

//From API
@property (nonatomic) NSInteger width;
@property (nonatomic) NSInteger height;
@property (nonatomic, strong) NSString *previewUrl;
@property (nonatomic, strong) NSString *fullUrl;
@property (nonatomic) NSInteger attachType;

//Uploaded
@property (nonatomic, strong) NSString *filename;
@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, strong) NSString *fileId;
@property float uploadProgress;

-(void)createWithDictionary:(NSDictionary *)dict;

@end
