//
//  AttachmentFactory.h
//  doctorFeed
//
//  Created by Artem Peskishev on 19.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Attachment.h"

typedef NS_ENUM(NSInteger, AttachmentType) {
    PhotoType,
    VideoType
};

@interface AttachmentFactory : NSObject

+ (Attachment *)initWithDictionary:(NSDictionary *)dict attachmentType:(AttachmentType)type;

@end
