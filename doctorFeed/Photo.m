//
//  Photo.m
//  doctorFeed
//
//  Created by Artem Peskishev on 07.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "Photo.h"

@implementation Photo

- (void)createWithDictionary:(NSDictionary *)dict
{
    [super createWithDictionary:dict];

    self.attachType = PhotoType;
    self.width = [dict[@"width"] integerValue];
    self.height = [dict[@"height"] integerValue];
}

@end
