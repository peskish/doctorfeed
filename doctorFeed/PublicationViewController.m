//
//  PublicationViewController.m
//  doctorFeed
//
//  Created by Artem Peskishev on 20.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "PublicationViewController.h"
#import <UIImageView+WebCache.h>
#import "NSAttributedString+DDHTML.h"
#import "IndexedCollectionView.h"
#import "AttachmentCollectionViewCell.h"
#import "Photo.h"
#import "PublicationCommentTableViewCell.h"

#import <IDMPhoto.h>
#import <IDMPhotoBrowser.h>

#import "PublicationFactory.h"
#import "Publication.h"

@interface PublicationViewController ()
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *authorAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *authorSpecialtyLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *publicationCreatedAtLabel;
@property (weak, nonatomic) IBOutlet UILabel *publicationTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *publicationContentLabel;
@property (weak, nonatomic) IBOutlet UIButton *recommendButton;
@property (weak, nonatomic) IBOutlet UILabel *recommendationsCountLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photosCollectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerBottomLayoutConstraint;

@property (nonatomic, strong) NSMutableArray *commentsArray;
@end

@implementation PublicationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showFullComment:)
                                                 name:@"showFullComment"
                                               object:nil];
    
    self.commentsArray = [NSMutableArray new];
    
    [self.authorAvatarImageView sd_setImageWithURL:[NSURL URLWithString:self.publication.author.avatarUrl]];
    [self.authorSpecialtyLabel setText:self.publication.author.specialty];
    [self.authorNameLabel setText:self.publication.author.shortName];
    [self.publicationCreatedAtLabel setText:[Helper formatDate:self.publication.createdAt]];
    [self.publicationTitleLabel setText:self.publication.title];
    self.authorAvatarImageView.layer.cornerRadius = 4;
    self.recommendButton.layer.cornerRadius = 4;
    self.contentView.layer.cornerRadius = 4;
    self.headerBottomLayoutConstraint.constant = 4;

    if ((self.publication.recommendedByDoctor)||(!self.publication.canRecommend))
    {
        self.recommendButton.hidden = YES;
    }
    
    [SVProgressHUD showWithStatus:@"Загрузка публикации"];
    
    [[APIManager sharedManager]loadPublication:self.publication.publicationID
                               completionBlock:^(NSDictionary *answer, NSError *error) {
                                   self.publication.content = answer[@"publication"][@"content"];
                                   [self setPublicationHeaderContent];
                                   [self setPublicationHeaderHeight];
                                   [SVProgressHUD dismiss];
                                   
                                   [[APIManager sharedManager] loadCommentsForPublication:self.publication.publicationID
                                                                                  endDate:nil
                                                                                    count:@"20"
                                                                          completionBlock:^(NSDictionary *answer, NSError *error) {
                                                                              for (NSDictionary *commentDict in answer[@"comments"])
                                                                              {
                                                                                  Publication *comment = [PublicationFactory createPublication:CommentType withDict:commentDict];
                                                                                  [self.commentsArray insertObject:comment atIndex:0];
                                                                              }
                                                                              
                                                                              if (self.commentsArray.count)
                                                                              {
                                                                                  self.headerBottomLayoutConstraint.constant = -5;
                                                                              }
                                                                              
                                                                              [self.tableView reloadData];
                               }];
    }];
    
}

- (void)setPublicationHeaderContent
{
    if (self.publication.canRecommend)
    {
        self.recommendationsCountLabel.hidden = YES;
    } else {
        self.recommendButton.hidden = YES;
    }
    
    if (self.publication.photosArray.count)
    {
        self.photosCollectionViewHeightConstraint.constant = [Helper heightForPhotosCollectionsView:self.publication];
    } else {
        self.photosCollectionViewHeightConstraint.constant = 0;
    }
}

- (void)setPublicationHeaderHeight
{
    NSAttributedString *attributedString = [NSAttributedString attributedStringFromHTML:self.publication.content];
    NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc]initWithAttributedString:attributedString];
    [mutableString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(0, mutableString.length)];

    [self.publicationContentLabel setAttributedText:mutableString];
    
    float stringHeight = [Helper findHeightForText:attributedString.string havingWidth:self.view.frame.size.width-20 andFont:[UIFont systemFontOfSize:13]].height + 60;
    
    stringHeight += [Helper findHeightForText:self.publication.title havingWidth:self.view.frame.size.width-17 andFont:[UIFont boldSystemFontOfSize:13]].height + 5;
    
    if (self.publication.photosArray.count) stringHeight += [Helper heightForPhotosCollectionsView:self.publication] + 2;
    
    if (self.publication.canRecommend)
    {
        self.recommendationsCountLabel.hidden = YES;
        stringHeight += 40;
    } else {
        self.recommendButton.hidden = YES;
        stringHeight += 5;
    }
    
    CGRect frame = self.headerView.frame;
    frame.size.height = stringHeight;
    self.headerView.frame = frame;
        
    self.tableView.tableHeaderView = self.headerView;
}

//==================------------------
#pragma mark - Buttons
//==================------------------

- (IBAction)recommendPublicationButtonTapped:(id)sender
{
    [SVProgressHUD showWithStatus:@"Оставляем рекомендацию"];
    [[APIManager sharedManager] recommendPublication:self.publication.publicationID completionBlock:^(NSDictionary *answer, NSError *error) {
        self.recommendButton.hidden = YES;
        [SVProgressHUD showSuccessWithStatus:@"Успешно"];
    }];
}

//==================------------------
#pragma mark - Notifications
//==================------------------

- (void)showFullComment:(NSNotification *)notification
{
    Publication *comment = self.commentsArray[[notification.object integerValue]];
    comment.showFullComment = YES;

    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:[notification.object integerValue] inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

//==================------------------
#pragma mark - TableView
//==================------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.commentsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"PublicationCommentCell";
    
    PublicationCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Publication *comment = self.commentsArray[indexPath.row];
    
    if (indexPath.row == self.commentsArray.count - 1)
    {
        cell.whiteBackgroundBottomConstraint.constant = 5;
        cell.whiteBackgroundView.layer.cornerRadius = 4;
        cell.commentViewBottomConstraint.constant = 8;
    } else {
        cell.whiteBackgroundBottomConstraint.constant = -4;
        cell.whiteBackgroundView.layer.cornerRadius = 0;
        cell.commentViewBottomConstraint.constant = 2;

    }
    
    cell.commentIndex = [self.commentsArray indexOfObject:comment];
    
    [cell setCell:comment];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Publication *comment = self.commentsArray[indexPath.row];
    NSAttributedString *attributedString = [NSAttributedString new];
    
    if (comment.showFullComment)
    {
        attributedString = [NSAttributedString attributedStringFromHTML:comment.content];
    } else {
        attributedString = [NSAttributedString attributedStringFromHTML:comment.annotation];
    }
    
    float stringHeight = [Helper findHeightForText:attributedString.string havingWidth:self.view.frame.size.width-32 andFont:[UIFont systemFontOfSize:13]].height + 85;
    
    
    if ((comment.hasFullContent)&&(!comment.showFullComment)) stringHeight += 25;
    
    if (comment.photosArray.count) stringHeight += [Helper heightForPhotosCollectionsView:comment] + 2;
    
    if (indexPath.row == self.commentsArray.count -1 )
    {
        stringHeight += 5;
    }
    
    return stringHeight + 10;
    
}


//==================------------------
#pragma mark - Indexed Collection View
//==================------------------

-(NSInteger)collectionView:(IndexedCollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    
    Publication *publication = [Publication new];
    
    if (collectionView.tag)
    {
        publication = self.publication;
    } else {
        publication = self.commentsArray[collectionView.index];
    }
    
    return publication.photosArray.count;
}

-(UICollectionViewCell *)collectionView:(IndexedCollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AttachmentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCollectionViewCell" forIndexPath:indexPath];
    
    Publication *publication = [Publication new];
    
    if (collectionView.tag)
    {
        publication = self.publication;
    } else {
        publication = self.commentsArray[collectionView.index];
    }
    
    Photo * photoToShow = publication.photosArray[indexPath.item];
    
    [cell.photoImageView sd_setImageWithURL:[NSURL URLWithString:photoToShow.previewUrl]];
    cell.photoImageView.layer.cornerRadius = 4;
    cell.photoImageView.clipsToBounds = YES;
    return cell;
}

-(CGSize)collectionView:(IndexedCollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
#warning ЖДУ КОГДА ЖЕНЯ ПОПРАВИТ РАЗРЕШЕНИЕ ФОТОК С СЕРВЕРА
    Publication *publication = [Publication new];
    
    if (collectionView.tag)
    {
        publication = self.publication;
    } else {
        publication = self.commentsArray[collectionView.index];
    }
    
    Photo * photoToShow = publication.photosArray[indexPath.item];
    CGSize itemSize;
    
    switch (publication.photosArray.count) {
        case 0:
            
            break;
        case 1:
            itemSize.height = [[UIScreen mainScreen] bounds].size.height*2/3;
            if (photoToShow.width > self.view.frame.size.width)
            {
                itemSize.width = self.view.frame.size.width;
                itemSize.height = photoToShow.height * itemSize.width / photoToShow.width;
            }
            break;
            
        default:
            itemSize.height = [[UIScreen mainScreen] bounds].size.height/3;
            break;
    }
    
    if (itemSize.height > photoToShow.height)
    {
        itemSize.height = photoToShow.height;
    }
    
    
    
    itemSize.width = photoToShow.width * itemSize.height / photoToShow.height;
    return itemSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView
                        layout:(UICollectionViewLayout *) collectionViewLayout
        insetForSectionAtIndex:(NSInteger) section {
    
    return UIEdgeInsetsMake(0, 0, 0, 5); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *) collectionView
                   layout:(UICollectionViewLayout *) collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
    return 5.0;
}

-(void)collectionView:(IndexedCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Publication *publication = [Publication new];
    
    if (collectionView.tag)
    {
        publication = self.publication;
    } else {
        publication = self.commentsArray[collectionView.index];
    }
    
    NSMutableArray *photosArray = [[NSMutableArray alloc]init];
    for (Photo *photo in publication.photosArray)
    {
        IDMPhoto *photoForGallery = [IDMPhoto photoWithURL:[NSURL URLWithString:photo.fullUrl]];
        [photosArray addObject:photoForGallery];
    }
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photosArray];
    [browser setInitialPageIndex:indexPath.item];
    [self presentViewController:browser animated:YES completion:nil];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(PublicationCommentTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
}


@end
