//
//  AttachmentFactory.m
//  doctorFeed
//
//  Created by Artem Peskishev on 19.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "AttachmentFactory.h"
#import "Photo.h"
#import "Video.h"

@implementation AttachmentFactory

+(Attachment *)initWithDictionary:(NSDictionary *)dict attachmentType:(AttachmentType)type
{
    Attachment *attachment = nil;
    
    if (type == PhotoType) {
        
        attachment = [Photo new];
        
    } else if (type == VideoType) {
        
        attachment = [Video new];
        
    }
    
    [attachment createWithDictionary:dict];
    
    return attachment;
}

@end
