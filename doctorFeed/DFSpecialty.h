//
//  DFSpecialty.h
//  doctorFeed
//
//  Created by Artem Peskishev on 11.05.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DFSpecialty : NSObject

@property (nonatomic, strong) NSNumber *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *addressToDoctor1;
@property (nonatomic, strong) NSString *addressToDoctor2;
@property (nonatomic, strong) NSString *addressToDoctor3;
@property (nonatomic, strong) NSNumber *selectOrderIndex;

- (void)import:(NSDictionary *)dict;

@end
