//
//  PublicationFactory.m
//  doctorFeed
//
//  Created by Artem Peskishev on 19.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "PublicationFactory.h"
#import "FeedPublication.h"
#import "Comment.h"

@implementation PublicationFactory

+(Publication *)createPublication:(PublicationType)type withDict:(NSDictionary *)dict
{
    Publication *publication = nil;
    
    if (type == FeedType) {
        
        publication = [FeedPublication new];
        
    } else if (type == CommentType) {
        
        publication = [Comment new];
        
    } else if (type == AnswerType) {
        
    }
    
    [publication createWithDictionary:dict];
    
    return publication;
}

@end
