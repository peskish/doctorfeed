//
//  Comment.h
//  doctorFeed
//
//  Created by Artem Peskishev on 25.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : Publication

- (void)createWithDictionary:(NSDictionary *)dict;

@end
