//
//  Video.m
//  doctorFeed
//
//  Created by Artem Peskishev on 19.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "Video.h"

@implementation Video

-(void)createWithDictionary:(NSDictionary *)dict
{
    [super createWithDictionary:dict];
    self.attachType = VideoType;
    self.width = 240;
    self.height = 180;

}

@end
