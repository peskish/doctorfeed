//
//  CustomNavigationController.h
//  doctorFeed
//
//  Created by Artem Peskishev on 30.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController

@end
