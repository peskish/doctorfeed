//
//  PublicationFactory.h
//  doctorFeed
//
//  Created by Artem Peskishev on 19.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Publication.h"

typedef NS_ENUM(NSInteger, PublicationType) {
    FeedType,
    CommentType,
    AnswerType
};

@interface PublicationFactory : NSObject

+ (Publication *)createPublication:(PublicationType)type withDict:(NSDictionary *)dict;

@end
