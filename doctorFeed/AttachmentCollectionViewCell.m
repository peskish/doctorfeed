//
//  PhotoCollectionViewCell.m
//  doctorFeed
//
//  Created by Artem Peskishev on 09.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import "AttachmentCollectionViewCell.h"

@interface AttachmentCollectionViewCell ()
@property (nonatomic, strong) NSString *videoURL;
@end

@implementation AttachmentCollectionViewCell

-(void)awakeFromNib
{
    self.photoImageView.layer.cornerRadius = 4;
}

- (void)setCellWithAttachment:(Attachment *)attachment
{
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:attachment.previewUrl]
                           placeholderImage:[UIImage imageNamed:@"image-placeholder"]];
    self.photoImageView.layer.cornerRadius = 4;
    self.photoImageView.clipsToBounds = YES;
    
    self.playVideoButton.hidden = YES;
    
    if (attachment.attachType == VideoType)
    {
        self.videoURL = attachment.fullUrl;
        self.playVideoButton.hidden = NO;
    }
}

- (IBAction)playVideoButtonTapped:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"playVideo" object:self.videoURL];
}
@end
