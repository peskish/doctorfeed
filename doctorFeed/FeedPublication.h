//
//  Publication.h
//  doctorFeed
//
//  Created by Artem Peskishev on 05.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Doctor.h"
#import "Publication.h"

@interface FeedPublication : Publication


- (void)createWithDictionary:(NSDictionary *)dict;

@end
