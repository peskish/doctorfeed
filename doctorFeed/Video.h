//
//  Video.h
//  doctorFeed
//
//  Created by Artem Peskishev on 19.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Attachment.h"

@interface Video : Attachment

-(void)createWithDictionary:(NSDictionary *)dict;

@end
