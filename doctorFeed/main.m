//
//  main.m
//  doctorFeed
//
//  Created by Artem Peskishev on 05.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
