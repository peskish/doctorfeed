//
//  DFSpecialty.m
//  doctorFeed
//
//  Created by Artem Peskishev on 11.05.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "DFSpecialty.h"


@implementation DFSpecialty



- (void)import:(NSDictionary *)dict
{
    self.identifier = [dict objectForKey:@"id"];
    self.name = [dict objectForKey:@"name"];
    self.addressToDoctor1 = [dict objectForKey:@"addressToDoctor1"];
    self.addressToDoctor2 = [dict objectForKey:@"addressToDoctor2"];
    self.addressToDoctor3 = [dict objectForKey:@"addressToDoctor3"];
    self.selectOrderIndex = [dict objectForKey:@"selectOrderIndex"];
}


@end
