//
//  PublicationViewController.h
//  doctorFeed
//
//  Created by Artem Peskishev on 20.11.15.
//  Copyright © 2015 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublicationViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property Publication *publication;

@end
