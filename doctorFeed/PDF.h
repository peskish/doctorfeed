//
//  PDF.h
//  doctorFeed
//
//  Created by Artem Peskishev on 20.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDF : NSObject

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *title;
@property (nonatomic) NSInteger size;

- (void)createWithDict:(NSDictionary *)dict;

@end
