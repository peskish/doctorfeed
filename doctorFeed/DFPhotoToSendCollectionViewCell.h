//
//  DFPhotoToSendCollectionViewCell.h
//  doctorFeed
//
//  Created by Artem Peskishev on 13.05.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Attachment.h"


@interface DFPhotoToSendCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (strong, nonatomic) Attachment *attachment;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (strong, nonatomic) NSString *videoUrl;

- (void)setCellWithAttachment:(Attachment *)attachment;

- (void)setCellWithVideo:(NSString *)videoURL;

@end
